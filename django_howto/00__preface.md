# Django

In this tutorial you gonna be learning how to built a full featured web
application using the django framework in python. Django is a very popular
framework. It gives you a lot of functionality right out of the box.

## What is Django
Django is a high-level Python Web framework that encourages rapid development
and clean, pragmatic design. Built by experienced developers, it takes care of
much of the hassle of Web development, so you can focus on writing your app
without needing to reinvent the wheel. It’s free and open source.

## What you will learn

In this tutorial you gonna built a blog application for different users with
different accounts writing multiple posts. You gonna have an authentication
system with the ability to register a new accounts for new users. If you have an
account you can login where you will be able to reset your password in case you forgot it.
The blog app will then send you an email. If you are logged in, you can
update your profile information including the profile picture. You can view
posts of other people. You can read, delete and update your own posts.

This is probably a great way to learn the ins and outs of a framework. Because
you gonna be exposed to many different things. For example you will learn how
to work with databases, to create an authentication system, accept user input
from forms, send emails to reset passwords. With the django framework you have
the ability to access an admin page. Within there you have a nice GUI to edit
posts too. All in all you gonna be adding a lot of functionality to your app.

## Resources

This tutorial is a transcription of the YouTube series of CoreyMS:
[Django Tutorials](https://www.youtube.com/watch?v=UmljXZIypDc&list=PL-osiE80TeTtoQCKZ03TU5fNfx2UY6U4)

