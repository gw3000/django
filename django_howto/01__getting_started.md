# 01 Getting started

This howto describes how to start a little django project from scratch. You can use it in Linux, MacOS X or MS Windows. The prerequisites are:

* python (Version 3.6 or higher)
* django (Version 2.1 or higher)

You should be familiar with the terminal, a modern web browser and an editor (sublime, vim, etc.).Basic knowledge in python would be fine.

## Create a virtual environment

Create a new project-folder:

```bash
mkdir django
cd django
```


From the Terminal you can use the virtual environment module from python. To start a new virtual Environment type in the terminal the following:

```bash
python -m venv django_venv
```

To use the Environment do this:

```bash
source django_venv/bin/activate
```

The prompt of the terminal should look like this:

```bash
(django_venv) ➜  django
```

## Install Django

After Installing and entering the virtual environment, you can install the django package in the venv with __pip__.

```bash
pip install django
```

After downloading the package, you start a project with the following line:

```bash
django-admin startproject django_project
```

With a look inside the generated folders:

```bash
cd django_project
tree
.
├── django_project
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── manage.py
```

## The starting and stopping the django-web server

Run the server with this command:

```bash
python manage.py runserver
```

With the last command you started the web server running the generated website. To enter the page with a web browser you can copy the URL from the terminal and paste it into the browser:

```html
http://127.0.0.1:8000/
```

Now you see a login page to the Django site admin page!

To stop the django-server just hit ```CTRL-C``` in the terminal!


## The and Admin-URL

The server delivers the page on port 8000. To enter the admin page how have to type this url:

```html
http://localhost:8000/admin
```
