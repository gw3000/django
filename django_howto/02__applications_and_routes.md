# 02 Applications and Routes

In this section you create a blog app to your website - project. In a django - website - project you can have multiple apps. For instance you can have a blog section, a store - section. A single project can contain multiple apps.

# The blog app

To create a blog app in your current django - project you have to type the following:

```bash
python manage.py startapp blog
```

If you take a look inside the directory and file structure of your project you´ll see an added directory.

```bash
.
├── blog
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── migrations
│   │   └── __init__.py
│   ├── models.py
│   ├── tests.py
│   └── views.py
├── db.sqlite3
├── django_project
│   ├── __init__.py
│   ├── __pycache__
│   │   ├── __init__.cpython - 36.pyc
│   │   ├── settings.cpython - 36.pyc
│   │   ├── urls.cpython - 36.pyc
│   │   └── wsgi.cpython - 36.pyc
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── manage.py
```

From here open the __views.py__ file in the blog directory and add the following:

```python
from django.http import HttpResponse
```

At next you have to add a __funtion__ in the views.py file called __home__ to handle the traffic from the homepage of your blog. The function is gonna take in a request argument. Within this function you are gonna return what you want the user will see.

```python


def home(request):
    return HttpResponse('<h1>Blog Home</h1>')


```
This is the logic how you want to handle, when a user goes to the blog homepage. You haven´t actually mapped the URL pattern to view function just yet. To do this, you have to create a new blog module called __urls.py__. In that file you map the URLs to the view function.

```bash
cd blog
touch urls.py
```

Add the following to the URLs.py:

```python
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='blog-home'),
]
```

__views.home__ was created in views.py as a function __home__


In the urls.py file in the django_project folder, you have to import the __include__ function. In the URL patterns you have to add the blog reference with the __include funtion__.

```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('blog/', include('blog.urls')),
]
```
If you run the server now, the original root page does not exist anymore. The debug output looks like this:

```bash
Not Found: /
[01 / Sep / 2018 15:40:58] "GET / HTTP/1.1" 404 2034
```

The website itself shows the following output:

```
Using the URLconf defined in django_project.urls, Django tried these URL patterns, in this order:

    admin/
    blog/

The empty path didn't match any of these.
```
# Adding an about page

To simple add an about page to the blog you have to do the following:

* adding an __about__ entry in the __urls.py__:

```python
from django.urls import path
from . import views

urlpatterns = [
    # views.home was created in views.py as a function home
    path('', views.home, name='blog-home'),
    path('about', views.about, name='blog-about'),
]
```
* adding a about function in the __views.py__:
 ```python
 from django.shortcuts import render
from django.http import HttpResponse


def home(request):
    return HttpResponse('<h1>Blog Home</h1>')


def about(request):
    return HttpResponse('<h1>Blog About</h1>')
```

## Default URL

To make the blog page the default URL of your project you have to move the project __url.py__:

```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('blog.urls')),
]
```
