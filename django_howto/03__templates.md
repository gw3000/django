# 03 Templates

Templates are capable of returning more complex html code. You can pass variables in templates.

# Folder and file structure

You need to create a templates folder in your blog app directory.

```bash
cd blog
mkdir templates
```

By default django looks from the templates subdirectory for each of your installed apps. (We haven't mentioned installed apps so far!)
Django looks for the app specific templates in the template subdirs. So you have to add another subdirs in the templates folder:

```bash
cd templates
mkdir blog
```

In this directory you can add your templates like:

```bash
cd blog
touch home.html
touch about.html
```

Now you can edit your home.html page like this:

```html
<!DOCTYPE html >
<html lang = "en" >
<head >
    <meta charset = "UTF-8" >
    <title > Blog < /title >
</head >
<body >
<h1 > Blog home!< /h1 >
</body >
</html >
```

... and your about.html page like this:

```html
<!DOCTYPE html >
<html lang = "en" >
<head >
    <meta charset = "UTF-8" >
    <title > Blog < /title >
</head >
<body >
<h1 > Blog About < /h1 >
</body >
</html >
```

# Installed apps

In the __apps.py__ module you have to copy the __BlogConfig__ name

```python
class BlogConfig(AppConfig):
    name = 'blog'
```
... to the django_project.settings.py file(__'blog.apps.BlogConfig', __):

```python
# Application definition

INSTALLED_APPS = [
    'blog.apps.BlogConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```


# Loading the templates

Django renders templates! So you have to insert the module that will the rendering. In the __blog > views.py__ module you can already find this import module thing:

```python
from django.shortcuts import render
```
To render the template the __views.py__ should look like this(have a look at the __home__ section):

```python
from django.shortcuts import render
from django.http import HttpResponse


def home(request):
    return render(request, 'blog/home.html')


def about(request):
    return HttpResponse('<h1>Blog About</h1>')
```

To make this also work for the about page you have to add the rendering in the about section and you can remove the __import__ option:

```python
from django.shortcuts import render


def home(request):
    return render(request, 'blog/home.html')


def about(request):
    return render(request, 'blog/about.html')
```

# Variables in templates

To see how it works you can add some dummy data to the blog views.py(__posts__):

```python
from django.shortcuts import render

posts = [
    {
        'author': 'gw',
        'title': 'First Blog post',
        'content': 'First post content',
        'date_posted': '2018-09-02'
    },
    {
        'author': 'cw',
        'title': 'Second Blog post',
        'content': 'Second post content',
        'date_posted': '2018-09-03'
    }
]


def home(request):
    return render(request, 'blog/home.html')


def about(request):
    return render(request, 'blog/about.html')
```

After that you have to put that dummy data into a dictionary within your home page view and add a third argument in home view:


```python
...


def home(request):
    context = {
        'posts' : posts
    }
    return render(request, 'blog/home.html', context)


...
```
Now we need to loop thru this post in your home.html template. The templating
engine in django is similar to jinja2 and it allows you to write code into the
templates you are using.

### for loops in templates

We need to write a for loop in our home.html template like this:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog</title>
</head>
<body>
    {% for post in posts %}
        <h1>{{ post.title }}</h1>
        <p>by {{ post.author }} on {{post.date_posted}}</p>
        <p>{{ post.content}}</p>
    {% endfor %}
</body>
</html>
```
So you can see you have to end the loop with an __endloop__. To access a
variable you have to use __{{}}__.

### if-else statements in templates
Look in the title section of the home.html template:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {% if title %}
        <title>Django Blog - {{title}}</title>
    {% else %}
        <title>Django Blog</title>
    {% endif %}
</head>
<body>
    {% for post in posts %}
        <h1>{{ post.title }}</h1>
        <p>by {{ post.author }} on {{post.date_posted}}</p>
        <p>{{ post.content}}</p>
    {% endfor %}
</body>
</html>
```
You can do the in the about.html template too. To view the right title in the
about.html page you have to update the views.py like this:

```python
def about(request):
    return render(request, 'blog/about.html', {'title': 'About-Page'})
```
## Prevent repeating

If you look into the home.html and the about.html template you will see similar
code. To prevent you from repeating (think of updating code in both templates,
you have to write redundant code), let´s add an additional template called
__base.html__ in the templates folder:

```bash
cd blog/templates/blog
touch base.html
```
... and edit the base.html like this:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {% if title %}
        <title>Django Blog - {{title}}</title>
    {% else %}
        <title>Django Blog</title>
    {% endif %}
</head>
<body>
    {% block content %}{% endblock %}
</body>
</html>
```
You can see that there is a __block__ section in the body section of the
template.html file.
Your home.html template should shortened like this:

```html
{% extends "blog/base.html" %}
{% block content %}
    {% for post in posts %}
        <h1>{{ post.title }}</h1>
        <p>by {{ post.author }} on {{post.date_posted}}</p>
        <p>{{ post.content}}</p>
    {% endfor %}
{% endblock content %}
```
Your about.html template should now look like this:

```html

{% extends "blog/base.html" %}
{% block content %}
    <h1>About Page</h1>
{% endblock content %}
```
## Bootstrap
After "__BOOTSTRAPPING__" your base.html template the file should look like
this:

```html

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    {% if title %}
        <title>Django Blog - {{title}}</title>
    {% else %}
        <title>Django Blog</title>
    {% endif %}
</head>
<body>

    <div class="container">
        {% block content %}{% endblock %}
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>
```

With a lot of snippet adding in the base.html to use bootstrap properly the base.html should now look like this:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    {% if title %}
        <title>Django Blog - {{title}}</title>
    {% else %}
        <title>Django Blog</title>
    {% endif %}
</head>
<body>

    <header class="site-header">
      <nav class="navbar navbar-expand-md navbar-dark bg-steel fixed-top">
        <div class="container">
          <a class="navbar-brand mr-4" href="/">Django Blog</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle" aria-controls="navbarToggle" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarToggle">
            <div class="navbar-nav mr-auto">
              <a class="nav-item nav-link" href="/">Home</a>
              <a class="nav-item nav-link" href="/about">About</a>
            </div>
            <!-- Navbar Right Side -->
            <div class="navbar-nav">
              <a class="nav-item nav-link" href="#">Login</a>
              <a class="nav-item nav-link" href="#">Register</a>
            </div>
          </div>
        </div>
      </nav>
    </header>

    <main role="main" class="container">
      <div class="row">
        <div class="col-md-8">
          {% block content %}{% endblock %}
        </div>
        <div class="col-md-4">
          <div class="content-section">
            <h3>Our Sidebar</h3>
            <p class='text-muted'>You can put any information here you'd like.
              <ul class="list-group">
                <li class="list-group-item list-group-item-light">Latest Posts</li>
                <li class="list-group-item list-group-item-light">Announcements</li>
                <li class="list-group-item list-group-item-light">Calendars</li>
                <li class="list-group-item list-group-item-light">etc</li>
              </ul>
            </p>
          </div>
        </div>
      </div>
    </main>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>
```

## CSS
To add more specific CSS code to the project you have to add a __static__ folder to the app folder:

```bash
mkdir static
cd static
mkdir blog
```

In the last generated folder you you can add your CSS file:

```bash
touch main.css
```

With the following content:

```css
body {
  background: #fafafa;
  color: #333333;
  margin-top: 5rem;
}

h1, h2, h3, h4, h5, h6 {
  color: #444444;
}

ul {
  margin: 0;
}

.bg-steel {
  background-color: #5f788a;
}

.site-header .navbar-nav .nav-link {
  color: #cbd5db;
}

.site-header .navbar-nav .nav-link:hover {
  color: #ffffff;
}

.site-header .navbar-nav .nav-link.active {
  font-weight: 500;
}

.content-section {
  background: #ffffff;
  padding: 10px 20px;
  border: 1px solid #dddddd;
  border-radius: 3px;
  margin-bottom: 20px;
}

.article-title {
  color: #444444;
}

a.article-title:hover {
  color: #428bca;
  text-decoration: none;
}

.article-content {
  white-space: pre-line;
}

.article-img {
  height: 65px;
  width: 65px;
  margin-right: 16px;
}

.article-metadata {
  padding-bottom: 1px;
  margin-bottom: 4px;
  border-bottom: 1px solid #e3e3e3
}

.article-metadata a:hover {
  color: #333;
  text-decoration: none;
}

.article-svg {
  width: 25px;
  height: 25px;
  vertical-align: middle;
}

.account-img {
  height: 125px;
  width: 125px;
  margin-right: 20px;
  margin-bottom: 16px;
}

.account-heading {
  font-size: 2.5rem;
}
```

Now you have to include the __main.css__ file to the base.html template right above the title section:

```html
 <link rel="stylesheet" href="{% static 'blog/main.css'%}">
```

Have a look at the very top of the file. Here you can see how load static content to you template:

```html
{% load static  %}
```

To beautify the Blog in the __home.html__ template you can switch the for loop to this:

```html
{% extends "blog/base.html" %}
{% block content %}
    {% for post in posts %}
        <article class="media content-section">
          <div class="media-body">
            <div class="article-metadata">
              <a class="mr-2" href="#">{{ post.author }}</a>
              <small class="text-muted">{{ post.date_posted }}</small>
            </div>
            <h2><a class="article-title" href="#">{{ post.title }}</a></h2>
            <p class="article-content">{{ post.content }}</p>
          </div>
        </article>
    {% endfor %}
{% endblock content %}
```

## URLs

Inside the __base.html__ template we still have hard-coded URL. To change this you have to do the following:

```html
<a class="nav-item nav-link" href="{% url 'blog-home' %}">Home</a>
<a class="nav-item nav-link" href="{% url 'blog-about' %}">About</a>
```

Now you don´t have hard-coded links!
