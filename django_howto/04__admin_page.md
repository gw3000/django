# 04 Admin Page

You still haven´t any adminuser in your blog app. So you have to create one
. At first stop the running server.

now create the user:

```python
python manage.py createsuperuser
```
If you run this command you´ll get a lot of errors! The last line of the error
messages is very helpful to solve the problem:

```bash
...
no such table: auth_user

```
The problem here is, that you haven´t create the database for using this
project. From here we have to do a few __migration__ commands. The first
migration will create the database. So the __auth_table__ will be created in
that first migration.

```python
python manage.oy makemigration
python manage.py migrate
```
Now you´ll see that the needed table was created

```bash
Applying contenttypes.0001_initial... __OK__
Applying auth.0001_initial... __OK__
Applying admin.0001_initial... __OK__
Applying admin.0002_logentry_remove_auto_add... __OK__
Applying admin.0003_logentry_add_action_flag_choices... __OK__
Applying contenttypes.0002_remove_content_type_name... __OK__
Applying auth.0002_alter_permission_name_max_length... __OK__
Applying auth.0003_alter_user_email_max_length... __OK__
Applying auth.0004_alter_user_username_opts... __OK__
Applying auth.0005_alter_user_last_login_null... __OK__
Applying auth.0006_require_contenttypes_0002... __OK__
Applying auth.0007_alter_validators_add_error_messages... __OK__
Applying auth.0008_alter_user_username_max_length... __OK__
Applying auth.0009_alter_user_last_name_max_length... __OK__
Applying sessions.0001_initial... __OK__
```
Now you can create the __superuser__ with:

```python
python manage.py createsuperuser
```
... and it looks like this:

```bash
Username (leave blank to use 'gw'):
Email address: gunther.weissenbaeck@kanzlei-mbd.local
Password:
Password (again):
Superuser created successfully.
```
So run your server again and try out the created superuser account! You should
be able to login in the admin section of your blog app.

From here you can manage users and groups very easily.
