# 06 User registration

In this section you will learn how to use forms and how to validate user
input to create a user registration page. The application you will create is
capable of a user creating an account, login  and make posts. The first part of
that process is to create the __registration page__ where a user can create an
account on the website and then being able to login and logout. The admin page
from the previous sections might be good enough if you're the only person
using the site. But if you want to make an application anyone else can sign up
to or will be using like Twitter or YouTube you'll have to have another way
for people to create an account and login to your page other than the admin
page. Because non of those sites (Twitter, YouTube, etc.) give you access to
the admin site.

## User Logic

How will be the user logic relate to our project as a whole? The user
account portion is going to have its own forms and templates and routes. That
is going to be logically separate from the __blog__ itself. The best thing to
here would probably be to create a new __app__ inside of our project where all
of that is going to be contained in its own section. That way you know to look
when you will update user related data.

## Create a new app

You have already seen how to add new apps in a previous section:

```bash
python manage.py startapp users
```

In the __django_project__ you created a subfolder called __users__, which
includes the __users app__ which will include all the logic to "handle" users.
From here you need to create a users __signup__ page for the frontend of your
website. But if you go ahead remember to add the new create app to your
project! This will be happened in the __settings.py__ file in the
__django_project__ folder:

```python
...

INSTALLED_APPS = [
    'blog.apps.BlogConfig',
    'users.apps.UsersConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
...
```

## The users view

Open the __views.py__ file in the users folder and create a __register__ view
and add the following code:

```python
from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm


def register(request):
    form = UserCreationForm()
    return render(request, 'users/register.html', {'form': form})
```

Within your users folder you can create a templates folder like you did it with
your blog app:

```bash
mkdir -p templates/users
cd templates/users
touch register.html
```
From here you want to extend the base template and then fill in the content
blog. Maybe you copy the content of an existing template (e.g. about.html from
the blog templates) to the __register.html__. From here you can see that you
extend the __blog/base.html__ template!!!
Within your copied version of the about page you'll add an htmlclass and a
form with the method __POST__.

Within the created form you need to add something called __csrf token__. This
is a hidden tag and it is something you need to add here. It's adding something
called __cross site reguest forgery token__. This will protect your form
against certain attacks. It's just some added security django requires. If you
don't have it, your form will not work! Once you have that token in place now
you can simply access the form variable you passed in to the context of this
template. But first put in a field set tag which is used to group related
elements in a form. Also add a legend for your form as well:

```html
{% extends "blog/base.html" %}
{% block content %}
    <div class="content-section">
        <form method="POST">
            {% csrf_token %}
            <fieldset class="form-goup">
                <legend class="border-bottomi mb-4">Join Today</legend>
                {{ form }}
                <div class="form-group">
                    <button class="btn btn-outline-info" type="submit">Sign Up</button>
                </div>
            </fieldset>
        </form>
        <div calss="border-top pt-3">
            <small class="text-muted">
                Already have an account? <a class="ml-2" href="#">Sign In</a>
            </small>
       </div>
    </div>
{% endblock content %}
```

From here you need to create a URL pattern that uses your register view so that
you can navigate to this page in the browser. In your blog app you created your
own URLs module for the blog and you could do the same thing here. You could do
the same thing here. ... not yet! Just import your view directly within your
projects urls.py module and create a URL pattern there. In the
projects/urls.py:

```python
...
from django.contrib import admin
from django.urls import path, include
from users import views as user_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('register', user_views.register, name='register'),
    path('', include('blog.urls')),
]
...
```

If you start the server and open the register page, it should look like this:

![raw_register_page](pix/register_01.png "raw register page")

This isn't really pretty?! For now there is a small change to make to make this
look slightly better. That is to use a form method called __as_p__ which will
render your form and paragraph tags. At least that will split this up into a
few more lines then it is now. So go to your __register__ template to the form
section and write it like this:

```html
{ form.as_p }}
```

That will render your form in paragraph tags:

![asp](pix/register_02.png "as_p register page")

You can see how by using the user creation form it gave us all out if the box!
You have a username, a password field and a password conformation field. It
also printing out all of the validation stuff the user needs (character limits,
password complexity etc.)

When you fill out the fields and click Sign Up it just redirects you to the
first field and no new account will be created. The reason why this is not
doing what it should do is this is still not performing a post request on your
register route with the form information you've submitted. If you are going
back to your register view (__users/views.py__) then you can see that any
request that comes in to this route will simply creating a form and rendering
that out to the template.

## Requests and validation

Earlier you set a post request to send data. With this post method you will
able to validate posted data out of the form. So you have to put in a check in
place. In the __views.py__ file you have to add:

```python
from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
    else:
        form = UserCreationForm()
    return render(request, 'users/register.html', {'form': form})
```
 From here you are gonna use a __flash message__ to show that you've received
valid data. A flash message is an easy way for you to send one time alerts to a
template that will only displayed once and will disappear on the next request.

> Kinds of messages:
> * messages.debug
> * messages.info
> * messages.success
> * messages.warning
> * messages.error

```python
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}!')
            return redirect('blog-home')
    else:
        form = UserCreationForm()
    return render(request, 'users/register.html', {'form': form})
```

To display the message you have to open your __base__ template from the blog folder
and place the following logic there:

```html
<div class="col-md-8">
    {% if messages  %}
        {% for message in messages %}
            <div class="alert alert-{{ message.tags }}">
                {{ message }}
            </div>
        {% endfor  %}
    {% endif  %}
  {% block content %}{% endblock %}
</div>
```

If everything works it should look like this after you clicked __Sign Up__.:

![access reg](pix/register_03.png "success register page")

The flash message should work. You didn't actually create an account for that
user, but our forms a validated correctly and giving you some kind of feedback.
These flash messages are nice because they are one time alerts. If you reload
the homepage from here the alert goes away. This is just a one time thing!

That current registration form already does have some validation. If you fill
in the fields incorrectly you already have information about what went wrong
(e.g. a user with that username already exists). If you take a look inside the
__views.py__ in the users folder, the validation process escaped to the line:

```python
return render(request, 'users/register.html', {'form': form})
```

Because you had no valid data!

So let's set up the registration page that the user is added to the database.

## Saving users to the database

After the validation you just have to save the posted data in your views.py
file:!

```python
if form.is_valid():
    form.save()
    username = form.cleaned_data.get('username')
```

If you now add a new user it will save all data! What you don't have in that
place is an email address like the other users we added before.

## Email in the registration page

To add a email field to the registration page you would not do this in the
__register.html__ template. To do this you have to first create a file where
you can put this new form. So create a new file in the users app directory:

```bash
cd django_project/users
touch forms.py
```

Within this file you are going to create your first form that inherits from the
__user creation__ form. It should look like this:

```python
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
```

So go back to the _views.py_ file and add the top let's inherit the form the
you just created:

```python
from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegisterForm


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}!')
            return redirect('blog-home')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})
```

You should now see an email field! If you create another user there should be
an email address after sending the registration data.


## CSS

The registration page still looks ugly! So let's fix this. Right now
the registration page doesn't have the bootstrap styles that match the rest of
your site. If there are also validation problems you should style the warnings
in red so that is more clear what the user has to change to succeed. You also
don't need all of this validation information so large. It can be a lot smaller
and a lot more muted. In terms of adding classes to your form fields there are
ways to set classes on your form fields outside of the __forms.py__. That would
mix the presentation with the backend logic. The styling should find placed
in the templates. You can do this with a third party plug-in for django called
__crispy forms__. You can put some simple tags in your templates that will
style your forms in a bootstrap fashion.

At first you need to install crispy forms:

```bash
pip install django-crispy-forms
```

After that you need to tell django that this is an installed app. Remember to
to do this in your __django_project/settings.py__ module.

```python
...
INSTALLED_APPS = [
    'blog.apps.BlogConfig',
    'users.apps.UsersConfig',
    'crispy_forms',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
...
```

Crispy forms works per default with bootstrap Version two, so you have to tell
django using bootstrap version four. At the very bottom of the settings.py
file you have to add the following:

```python
CRISPY_TEMPLATE_PACK='bootstrap4'
```

Now open the __register.html__ template and load __crispy__ and add the following
at the top of the file:

```html
{% extends "blog/base.html" %}
{% load crispy_forms_tags %}
{% block content %}
```

That will allow you to use the crispy filters in your forms. What you don't
need from here is the __.as_p__ method in ``` {{ form.as_p }} ``` line. So
remove it to ```{{ form }} ```

To use a filter in in crispy you have to add a pipe sign after form.

```html
{{ form|crispy }}
```

Run your webserver and see how it looks:
![crispy_register_page](pix/register_04.png "crispy register page")

Beautiful!

After filled in the form fields with already used or false data the form highlights
the problems underneath in red. So you have a nicely styled form that can
actually create users in the frontend and gives a nice validation feedback on
the frontend as well.
