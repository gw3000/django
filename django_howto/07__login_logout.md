# 07 Login and Logout System

In this section you gonna be learning to create a authentication system for your django
blog app. User should be able to login and logout including to access certain pages.

Django already has a lot of functionality to create such an login logout
system on the backend. You start with the default login views!

At first import these default login logout views within your __urls.py__:

```bash
cd django_project
vim urls.py
```

by importing the __views as auth_views__ module:

```python
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from users import views as user_views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('register', user_views.register, name='register'),
    path('', include('blog.urls')),
```

After that let's create a login and a logout view by:

```python
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from users import views as user_views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/', user_views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('', include('blog.urls')),
```

Both views a class based views. You haven't seen class based views yet. But you
will make some of your own later in the tutorial. The built in views for login
and logout that django gives you will handle the forms, the logic and all the
stuff for you. It's not gonna handle templates. Which is good because you want
to make the templates anyways. You want them to match the look and style of
your current website.

If you run your web server from here you and surf to the login page you will get
an error message __TemplateDoesNotExist__. These errors are extremely useful in
debug mode, cause they point you in the directions what changes you need to
make in order to get things working. The error message here shows that a
template at __registration/login.html__ is not found. That's where that django
login view looks for that template by default. You could create a registration
directory inside of your templates and create a login.html template there. But
it makes more sense just to have your login template inside of your user
templates alongside your register page you've already created. You can tell
django to just look there instead. To do this open again the __urls.py__ and at
the end you can tell django where to look for a template. You pass these as an
argument for the __as_view()__ function in:


```python
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
```

If you reload your page you'll get the following message:

```html
Exception Value: users/login.html
```

Within your users template:

```bash
cd django_project/users/templates/users
```

You can copy the __register.html__ template to an __login.html__ template:

```bash
cp register.html login.html
```

Now you can edit the login.html template like:


```html

{% extends "blog/base.html" %}
{% load crispy_forms_tags %}
{% block content %}
    <div class="content-section">
        <form method="POST">
            {% csrf_token %}
            <fieldset class="form-goup">
                <legend class="border-bottomi mb-4">Log In</legend>
                {{ form|crispy }}
                <div class="form-group">
                    <button class="btn btn-outline-info" type="submit">Login</button>
                </div>
            </fieldset>
        </form>
        <div calss="border-top pt-3">
            <small class="text-muted">
                Need an account? <a class="ml-2" href="{% url 'register' %}">Sign Up Now</a>
            </small>
       </div>
    </div>
{% endblock content %}

```

You see that you have to rename the headline of the template to __Log In__,
the submit button to __Login__ and the __href__ will get a real link to __{% url 'register' %}__.
Now your login page should look like this:

![login_page](pix/login_01.png "login page")

You can also add the URL to the __register.html__ template:

```html
<div calss="border-top pt-3">
    <small class="text-muted">
        Already have an account? <a class="ml-2" href="{% url 'login' %}">Sign In</a>
    </small>
/div>
```

Now you have your login template done.

## Adding the functionality to the Login

If you fill in your login form with data that doesn't match any user or
password in your system the form responded correctly with the hint __Please
enter a correct username and password....__

If you try to login with an existing system account you gonna see another
error:


![login_page_error](pix/login_02.png "login page error")

You've got a __404__ error which means that its looking for a route that
doesn't exist. This isn't just a template that doesn't exist, it's trying to
access a URL that doesn't have a view attached to it. The URL that it's trying
to access is the:

```html
http://localhost:8000/accounts/profile/
```

You gonna add a __profile route__ to your site soon (but right now you don't have one).
And even if you did you don't want to redirect a user to there user profile
after they logged in. They should be redirected to the homepage when they
logged in. The reason why the site wants to redirect the user to the accounts
profile page is because django is just setted up so that when the login is
successful it tries to navigate to that location.

You can modify that location using your __settings.py__:

```bash
 cd django_project/
```

And go to the very bottom to create a new setting:

```python
...
CRISPY_TEMPLATE_PACK = 'bootstrap4'

LOGIN_REDIRECT_URL = 'blog-home'
```

If you want to login you'll land on the home page of your site. But there
isn't much visual feedback right now that tells you that you're logged in.
You'll fix that in just a second. If you go from here to the __admin__ page you
can see that you're logged in.

Before you go ahead let's change the __register.html__ redirection to the
__login__ page after registration:

```python
from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegisterForm


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Your account has been created! Your are now able to login.')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})
```
## Logout

To logout correctly you have to add an __logout.html__ template to the users
app. So copy the register.html template from the users folder to the
__logout.html__ template. You don't need any forms and form tags in the logout template so
you can delete it from the document. The logout.html template should now look
like this:

```html
{% extends "blog/base.html" %}
{% block content %}
    <h2>You have been logged out</h2>
    <div calss="border-top pt-3">
        <small class="text-muted">
            <a href="{% url 'login' %}">Log In again</a>
        </small>
   </div>
{% endblock content %}
```
From here let's check if the login/logout complex is working!

## The navigation bar

From here let's change the navigation bar, so that you can see if someone is
logged in and who is logged in! If some isn't logged in, he needs a login link.
This will find place in the __base.html__ template with a condition if someone
is logged in or logged out.

So from here go to the __base.html__ template folder and open up the file:

```bash
 cd django_project/blog/templates/blog/
```

In the __Navbar__ section change the routes to the following:


```html
<!-- Navbar Right Side -->
<div class="navbar-nav">
    <a class="nav-item nav-link" href="{% url 'login' %}">Login</a>
  <a class="nav-item nav-link" href="{% url 'register' %}">Register</a>
</div>
```

But if a user is logged in, he don't need the __register__ link! So let's do
that. Django can do this with and already existing condition called
__user.is_authenticated__. So the result could look like this:


```html
<!-- Navbar Right Side -->
<div class="navbar-nav">
    {% if user.is_authenticated %}
        <a class="nav-item nav-link" href="{% url 'logout' %}">Logout</a>
    {% else %}
        <a class="nav-item nav-link" href="{% url 'login' %}">Login</a>
        <a class="nav-item nav-link" href="{% url 'register' %}">Register</a>
    {% endif %}
</div>
```

## Restriction on certain routes

How to put restriction on certain routes, so that you can only go to this
routes if you are currently logged in. You see this on certain sites all the
time. So say that you click on a link to enter your twitter profile and you are
not logged in then it will first take you to the login page and say you have to
login first before you can view this page.

So let's create a route for the users profile that they can access after
they've logged in.

//First let's create this route to the profile. Open your user route __urls.py__ in the
__django_project__ folder

You should create a page for a users profile. First let's create that view
within the __users/views.py__ (currently you have your register view there):

```bash
cd django_project/users/templates/users
cp login.html profile.html
```

Delete the no necessary stuff and add the following:

```html
{% extends "blog/base.html" %}
{% load crispy_forms_tags %}
{% block content %}
    <h1>{{ user.username }}</h1>
{% endblock content %}
```

So let's create the route in your URL patterns that will use this view. In your
projects __urls.py__:


```python
...
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from users import views as user_views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/', user_views.register, name='register'),
    path('profile/', user_views.profile, name='profile'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
    path('', include('blog.urls')),
]
```

Let's add a link to the navigation bar in the __base.html__ template if the user is logged in.

```html
<!-- Navbar Right Side -->
<div class="navbar-nav">
    {% if user.is_authenticated %}
        <a class="nav-item nav-link" href="{% url 'profile' %}">Profile</a>
        <a class="nav-item nav-link" href="{% url 'logout' %}">Logout</a>
    {% else %}
        <a class="nav-item nav-link" href="{% url 'login' %}">Login</a>
        <a class="nav-item nav-link" href="{% url 'register' %}">Register</a>
    {% endif %}
</div>
```

So check if the login page is available after you've logged in:

![login_page_profile](pix/login_03.png "login page profile")

If you logout out from here and go back to the profile page, you can see that
you don't get anything on the screen because it doesn't have any current user
and doesn't know which username to display. So you want to make a check-in place
that makes a user login before they can access this page. To do this you need a
login required decorator that django already provides for you:

```bash
cd django_project/users/
```

Open the __views.py__ file and do the following:

```python

from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegisterForm
from django.contrib.auth.decorators import login_required


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Your account has been created! Your are now able to login.')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})

@login_required
def profile(request):
    return render(request, 'users/profile.html')
```

If you reload that same profile page not being logged in it looks like this:

![login_page_profile_error](pix/login_04.png "login page profile error")

The error says that the page its looking for doesn't exist. And its looking for
that page in

```html
http://localhost:8000/accounts/login/?next=/profile/
```

That's the default location django is looking in for routes. You need to tell
django where to find the login route. You do that in the __settings.py__ file in
your project folder:

```python

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'

CRISPY_TEMPLATE_PACK = 'bootstrap4'

LOGIN_REDIRECT_URL = 'blog-home'
LOGIN_URL = 'login'
```

If you reload the profile page now you will be redirected to the login page!

There's one really nice built in feature here . If you have a look in the URL
you was trying to access the profile page and it redirected you "you have to
log in first!". A parameter in the URL says what is next, after you logged in!
It's the profile page! Most of the users expect such a functionality!
