# 08 The user profile page

In this section you gonna continue to work on the user profile page. You do
this by extending the user model that you currently have to add more fields.
You also gonna learn how to use django signals to run specific functions after
certain actions which can be extremely useful for this type of work.

The default users model that django provides doesn't have a field for a profile
picture. So how can you add that? In order to do that you gonna have to extend
the user model and create a new profile model that has a one to one
relationship with the user. A one to one relationship means that one user can
have one profile and one profile will be associated with one user. So create a
new user model in your users app:

```bash
 cd django_project/users/
```

First of all you will extending the existing user model that django provides
for you. So you need to import that:

```python
from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    def __str__(self):
        return f'{self.user.username} Profile'
```

From here you have to migrate your database:

```bash
python manage.py migrate
```

You'll get an error message:

```bash
SystemCheckError: System check identified some issues:

ERRORS:
users.Profile.image: (fields.E210) Cannot use ImageField because Pillow is not
installed.
        HINT: Get Pillow at https://pypi.org/project/Pillow/ or run command
"pip install Pillow".
```

The system says that you have to install __Pillow__ which is a library working
with images.

```bash
pip install Pillow
```

So rerun the __makemigration__ command and it should find a migrations file:

```bash
 users/migrations/0001_initial.py
```
Now you have to migrate the database with the migrate command:

```bash
python manage.py migrate
Operations to perform:
  Apply all migrations: admin, auth, blog, contenttypes, sessions, users
Running migrations:
  Applying users.0001_initial... OK
```

That should have applied the changes to the database.

## User profiles on the admin page

You should be able to view all user profiles on the admin page. So you need to
register this model within the admin file of your app. This is an easy step to
forget (in the users/admin.py):

```python
from django.contrib import admin
from .models import Profile

admin.site.register(Profile)
```

So run from here your development server and go to the admin page. Here you can
see that there is a profile section under the Users headline. If you click on
the __Profiles__ link you can that there are 0 profiles create. So let's create
one manually and add an image to it!

## User profiles in the shell

So stop the dev server and run the following command in the shell:

```bash
python manage.py shell
```

Import the following:

```python
>>>from django.contrib.auth.models import User
>>> user = User.objects.filter(username='gw').first()
>>> user
<User: gw>
>>> user.profile
<Profile: gw Profile>
>>> user.profile.image
<ImageFieldFile: profile_pics/AntiMonsterSpray_A_Mp9x447.jpg>
>>>user.profile.image.width
1920
>>>user.profile.image.url
'profile_pics/AntiMonsterSpray_A_Mp9x447.jpg'
```

Another user with no picture uploaded will look like this:

```python
>>> user = User.objects.filter(username='TestUser').first()
'default.jpg'
```

You have to upload a default.jpg later!

## Location of Media Folders

If you have a look in the project folder you will find a  **profile_pics** folder.
That is the name that you placed in the __upload_to__ parameter of the image
field in your __profile model__. This isn't the best location for this
pictures because if multiple models saving different kinds of images then they
clutter up your projects root directory with different image directories. So
let's change some settings of the locations where this images are saved.
You also need to change a couple of different settings so that your website can
find these images when you try to view them from within the browser.

So open up the __settings.py__ file in the django_project folder and define a
so called __media-root__:

```python
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

CRISPY_TEMPLATE_PACK = 'bootstrap4'

LOGIN_REDIRECT_URL = 'blog-home'
LOGIN_URL = 'login'
```

What you've done here is using the __os.path.join__ methode to make sure the
full path to that directory is created correctly no matter what operating
system you're on. The __BASE_DIR__ variable is a variable django created at the
top of your __settings.py__ file that specifies the location of the projects
base directory. So bassicaly this is just saying that the media root will be at
your projects base directory.

The __MEDIA_URL__ is the public URL of that directory. So bassically this how
you will access the media thru the browser.

Those changes won't be applied to the profiles you've already created. So you
need to go in and remove those profiles that you've created and recreate those
to get these change take effect.

In your admin page delete the two created profiles and recreated them! From
here you will find a media folder in the root directory of the project. In that
directory is a profile_pics directory with the uploaded profile images. The old
profile_pics folder cann be deleted now.

Now it's all about presenting the picture in the profile page. So open the
__template__ for the profile page __profile.html__ and ad more bootstrap
functionality:

```html
{% extends "blog/base.html" %}
{% load crispy_forms_tags %}
{% block content %}
    <div class="content-section">
        <div class="media">
            <img src="{{ user.profile.image.url }}" class="rounded circle account-img">
            <div class="media-body">
                <h2 class="account-head">{{ user.username }}</h2>
                <p class="text-secondary">{{ user.email }}</p>
            </div>
        </div>
        <!-- form here -->
    </div>
{% endblock content %}
```

After that there is still one more thing to do to get this work. You need to
add those media routes to your URL patterns. There are different ways to do
this in development and production.

### Development vs Production media URL

If you are in developper mode you can modify your __urls.py__ file like this:

```python
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from users import views as user_views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/', user_views.register, name='register'),
    path('profile/', user_views.profile, name='profile'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
    path('', include('blog.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```

So open up the profile page in your app! You can see your uploaded profile
picture. You still haven't a default profile picture! Do that by putting a
default image in the root media folder!

## Django Signal -- auto created profiles

Right now you have a pretty nice user system where users have profile pictures
and profile pages. But you wanna make shure for every new user that us created
they automaticly a profile as well, which will include the default picture as
well. The way this is setup right now, you have to got to the admin page and
create the profiles for each of your users. You don't wanna do that every time.

This is pretty easy to do with a thing called a __django signal__. You gonna
create a new file in the users app called __signals.py__. The created file
should look like this:

```python
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
from .models import Profile

@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    instance.profile.save()
```

There is one little step you have to do before this works. You have to import
your signals inside of the __ready function__ of your __users/apps.py__ file.
