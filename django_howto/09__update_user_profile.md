# 09 Update the user profile

In this section you will finish the user profile page. User should be able to
update their user information and user picture by themselves. The uploaded
picture should be automatically resized. so you prevent extremely large files 
on your system.

In order to update your user profile you are going to need some forms. You
created forms in previous sections when you created the user register form for
you register page. In the users app folder open up the __forms.py__ file and do
within here you are gonna create some additional forms You gonna create a
__model form__. A model form allows you to create a form that will work with a
specific database model. In this case you want a form that will update your
user model:

´´´python
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        field = ['image']
´´´
And update the __views.py__ file in the same directory:


´´´python
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Your account has been created! Your are now able to login.')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})


@login_required
def profile(request):
    u_form = UserUpdateForm
    p_form = ProfileUpdateForm()

    context = {
        'u_form': u_form,
        'p_form': p_form
    }

    return render(request, 'users/profile.html')
´´´

From here open up the __profile.html__ template and add the following:

´´´html
{% extends "blog/base.html" %}
{% load crispy_forms_tags %}
{% block content %}
    <div class="content-section">
        <div class="media">
            <img src="{{ user.profile.image.url }}" class="rounded circle account-img">
            <div class="media-body">
                <h2 class="account-head">{{ user.username }}</h2>
                <p class="text-secondary">{{ user.email }}</p>
            </div>
        </div>
        <form method="POST">
           {% csrf_token %}
           <fieldset class="form-goup">
               <legend class="border-bottomi mb-4">Join Today</legend>
               {{ form|crispy }}
               <div class="form-group">
                   <button class="btn btn-outline-info" type="submit">Sign Up</button>
               </div>
           </fieldset>
       </form>
    </div>
{% endblock content %}
´´´

paused at 6:00
