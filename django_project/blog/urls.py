from django.urls import path
from . import views

urlpatterns = [
    # views.home was created in views.py as a function home
    path('', views.home, name='blog-home'),
    path('about/', views.about, name='blog-about'),
]
